# BOM - Liste du matériel

| nom                    | description                                    | quantité |
|------------------------|------------------------------------------------|----------|
| moteur                 | moteur de direction assistée                   | 2        |
| Collecteurs tournants  | pour faire passer l'electricité sur le plateau | 2        |
| Joints tournants       | pour apporter de l'eau sur le plateau          | 2        |
| Roulements à billes    |                                                |          |
| carte de puissance     | sabertooth                                     |          |
| arduino                |                                                |          |
| boitier industriel     |                                                |          |
| alimentation 12V 1000W |                                                |          |
| cadre                  |                                                |          |
| peinture               |                                                |          |
| caisson                |                                                |          |
| quincaillerie          |                                                |          |

## Moteur

## Collecteur Tournant

L'objectif du collecteur tournant est de permettre le passage de l'électricité jusqu'au
centre du plateau.

Références: CTH2042-00P/06S
Diamètre intérieur: 20mm
Diamètre extérieur: 42mm
Longueur: 25,4mm
Circuits (5A): 6

![photo collecteur tournant](doc/images/collecteur_tournant_photo.png)

https://www.collecteur-tournant.fr/collecteur-tournant-arbre-creux-cth2042.php

![plan collecteur tournant](doc/images/collecteur_tournant.png)

## Joints tournants

L'objectif est d'apporter de l'eau sur le plateau.

Le débit et la pression sont faible, nous prenons donc une petite section: HD6 

https://fr.aliexpress.com/item/1005005964251025.html?spm=a2g0o.productlist.main.1.66d2n6Djn6DjY9&algo_pvid=5cb1b001-b31f-4412-a87d-21ce64f7f21f&algo_exp_id=5cb1b001-b31f-4412-a87d-21ce64f7f21f-0&pdp_npi=4%40dis%21EUR%2119.38%2119.39%21%21%2120.06%2120.07%21%40211b6c1717321399066457387e1740%2112000035077951550%21sea%21FR%214793298287%21X&curPageLogUid=HAEceuXy8ykz&utparam-url=scene%3Asearch%7Cquery_from%3A

![joint tournant](doc/images/joint_tournant_photo.png)
