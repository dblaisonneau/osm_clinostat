OSM Open Clinostat - Made in FabLab Lannion
==================

__English version [here](README.en.md)__

Ce projet est un fork du projet "Open Clinostat 3D" de la Fédération Open Space
Maker. Voici les pages du projet original:

- [La page du projet sur le site OpenSpaceMaker](https://www.federation-openspacemakers.com/fr/participer/projets/open-clinostat-3d/)
- [La page du fablab Plateforme C](https://fablabo.net/wiki/OPEN_CLINOSTAT_3D)

Choix techniques
-----------------

Par rapport au projet orignal, nous sommes parti sur la même bases:

- même budget (<1000€)
- même taille (3U^3)
- structure ALU
- Moteurs Nema17

Respecter l'idée de base, ___"permettre à chaque maker de pouvoir reproduire
simplement ce clinostat"___ a été notre objectif premier. C'est pourquoi,
l'assemblage ne requiert que très peu d'outils d'ateliers:

- Une scie (un poste stationnaire de découpe à ruban est le mieux)
- Une perceuse (à colonne)
- Une imprimante 3D
- Une découpeuse laser (optionnelle)

Cependant en fonction du matériel que nous avions de disponibles, et le
fournisseur équivalent au projet original n’ayant pas les même références,
il y a des différences:

- profilés alu plus rigides avec ce que nous avions en stock (45mm de section)
- les cages des moteurs ont donc été refaites pour nos profilés
- Nema 17 avec réduction 1:19 et pas 1:100
- cornières alu et boulons pour rigidifier les segments coupés par les moteurs
- pilotage des moteurs par Raspberry via python, et non arduino

Nous avons ajouté au modèle original:

- des logiciels de contrôle dans des conteneurs Docker
- une interface de commande via un bus MQTT connecté sur le wifi du Raspberry
- des pieds pliables pour faciliter le déplacement.
- un gyroscope bluetooth avec supervision par un programme python et envoi
  des données sur le bus MQTT. Les données sont donc récupérables par une
  application tierce.

Amélioration en cours/prévues
------------------------------

Parmis les améliorations nous avons listé:
- interface de commande web (en cours)
- visualisation Web de l'inclinaison (en cours mais buggée)
- un système simple de récupération des données expérimentales.

Pour aller plus loin
---------------

Voici les autres informations disponibles:
- [Assemblage](docs/README.md)
- [Shopping list](docs/shopping_list.md)
- [Montage Electronique](docs/electonic.md)
- [Logiciels](docker/README.md)
- [Pièces 3D](hardware/README.md)
