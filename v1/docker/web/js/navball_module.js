
import * as THREE from './three.js/three.module.js';

// import Stats from './three.js/examples/jsm/libs/stats.module.js';

import { OrbitControls } from './three.js/OrbitControls.js';
import { TGALoader } from './three.js/TGALoader.js';

var camera, scene, renderer, stats;

init();
animate();

function init() {

	var container = document.createElement( 'div' );
	document.body.appendChild( container );

	camera = new THREE.PerspectiveCamera( 45, window.innerWidth / window.innerHeight, 0.1, 2000 );
	camera.position.set( 0, 50, 250 );

	scene = new THREE.Scene();

	//

	var loader = new TGALoader();
	var geometry = new THREE.SphereGeometry( 50, 32, 32 );
	// var geometry = new THREE.BoxBufferGeometry( 50, 50, 50 );

	// add box 1 - grey8 texture

	// var texture1 = loader.load( 'textures/crate_grey8.tga' );
	var texture1 = loader.load( './images/navball.tga' );
	var material1 = new THREE.MeshPhongMaterial( { color: 0xffffff, map: texture1 } );

	var mesh1 = new THREE.Mesh( geometry, material1 );

	scene.add( mesh1 );

	//

	var ambientLight = new THREE.AmbientLight( 0xffffff, 0.4 );
	scene.add( ambientLight );

	var light = new THREE.DirectionalLight( 0xffffff, 1 );
	light.position.set( 1, 1, 1 );
	scene.add( light );

	//

	renderer = new THREE.WebGLRenderer( { antialias: true } );
	renderer.setPixelRatio( window.devicePixelRatio );
	renderer.setSize( window.innerWidth, window.innerHeight );
	container.appendChild( renderer.domElement );

	//

	var controls = new OrbitControls( camera, renderer.domElement );

	//

	// stats = new Stats();
	// container.appendChild( stats.dom );

	//

	window.addEventListener( 'resize', onWindowResize, false );

}

function onWindowResize() {

	camera.aspect = window.innerWidth / window.innerHeight;
	camera.updateProjectionMatrix();

	renderer.setSize( window.innerWidth, window.innerHeight );

}


function animate() {

		requestAnimationFrame( animate );

		scene.children[0].rotation.x = navball_x * Math.PI / 180;
		scene.children[0].rotation.y = navball_y * Math.PI / 180;
		scene.children[0].rotation.z = navball_z * Math.PI / 180;
		// console.log(scene)
		renderer.render( scene, camera );
	// requestAnimationFrame( animate );
	//
	// render();
	// stats.update();

}

function render() {

	renderer.render( scene, camera );

}
