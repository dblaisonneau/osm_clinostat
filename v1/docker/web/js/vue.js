var myvue = new Vue({
  el: '#motors',
  data: {
    cnx_msg: "",
    motors: [
      {
        icon: "images/unchecked.svg",
        speed: 0,
        enable: false,
      }, {
        icon: "images/unchecked.svg",
        speed: 0,
        enable: false,
      }
    ]
  },
  methods: {
    motor_switch(motor_id) {
      console.log("Toggle motor "+motor_id);
      this.motors[motor_id].enable = !(this.motors[motor_id].enable);
      this.motors[motor_id].speed = ((this.motors[motor_id].enable) ? 5 : 0);
      let un = ((this.motors[motor_id].enable) ? '' : 'un');
      this.motors[motor_id].icon = "images/"+un+"checked.svg";
      console.log(this.mqtt_client)
      publish(topic + '/motor'+(motor_id+1)+"/action",
                          ((this.motors[motor_id].enable) ? 'start' : 'stop'))
    },
    motor_speed_change(motor_id) {
      console.log("Speed change for motor "+motor_id+" to "+this.motors[motor_id].speed);
      publish(topic + '/motor'+(motor_id+1)+"/speed",
                          this.motors[motor_id].speed)
    }
  }
})
