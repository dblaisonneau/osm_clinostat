var port = 9001;
var host = window.location.hostname;
var topic = "stepper";
var mqtt_client;
myvue.cnx_msg = "Not connected to " + host + " 🔴"
// Called after form input is processed
function startConnect() {
    // Generate a random client ID
    clientID = "oms_web-" + parseInt(Math.random() * 100);

    // Print output for the user in the messages div
    console.log('Connecting to: ' + host + ' on port: ' + port);
    console.log('Using the following client value: ' + clientID);

    // Initialize new Paho client connection
    mqtt_client = new Paho.MQTT.Client(host, Number(port), clientID);

    // Set callback handlers
    mqtt_client.onConnectionLost = onConnectionLost;
    mqtt_client.onMessageArrived = onMessageArrived;

    // Connect the client, if successful, call onConnect function
    mqtt_client.connect({
        onSuccess: onConnect,
    });
}

// Called when the client connects
function onConnect() {
    // Print output for the user in the messages div
    console.log('Subscribing to: ' + topic + '/#');

    // Subscribe to the requested topic
    mqtt_client.subscribe(topic + '/#');

    message = new Paho.MQTT.Message("Hello from webclient");
    message.destinationName = "stepper";
    mqtt_client.send(message);

    myvue.cnx_msg = "Connected to " + host + " 🟢"
}

// Called when the client loses its connection
function onConnectionLost(responseObject) {
    console.log("onConnectionLost: Connection Lost");
    if (responseObject.errorCode !== 0) {
        console.log("onConnectionLost: " + responseObject.errorMessage);
    }
}

// Called when a message arrives
function onMessageArrived(message) {
    // console.log("onMessageArrived: Topic: " + message.destinationName + '  | ' + message.payloadString);
    if (message.destinationName == 'clinostat/angle'){
      vars = JSON.parse(message.payloadString)
      navball_x = vars[0]
      navball_z = vars[1]
      navball_y = vars[2]
      console.log(vars)
    }
}

// Called when the disconnection button is pressed
function startDisconnect() {
    mqtt_client.disconnect();
    console.log('Disconnected');
}

function publish(topic, payload){
  var message = new Paho.MQTT.Message(payload);
  message.destinationName = topic;
  message.qos = 0;
  message.retain = false;
  mqtt_client.send(message);
}

startConnect();
