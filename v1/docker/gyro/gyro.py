#/usr/bin/env python3

import bluetooth
import configparser
import sys
import logging
import paho.mqtt.client as mqtt


def connect_to_mqtt(client_name, host, port):
    client = mqtt.Client(client_name)
    client.connect(host, port=int(port))
    return client


def connect_to_bt(addr, port):

    sock = bluetooth.BluetoothSocket(bluetooth.RFCOMM)
    sock.connect((addr, int(port)))
    return sock


def scan_bt_devices(device_name):
    addr = None
    while addr is None:
        logging.info("Scanning bluetooth devices")
        nearby_devices = bluetooth.discover_devices(duration=4,
                                                    lookup_names=True,
                                                    flush_cache=True,
                                                    lookup_class=False)
        if nearby_devices:
            logging.info("Found devices:")
            for device in nearby_devices:
                logging.info("  - {} [{}]".format(device[1], device[0]))
                if device[1] == device_name:
                    addr = device[0]
                    return addr
            logging.warning("Device '{}' not found".format(device_name))

def read_data(sock, div, fact):
    raw = sock.recv(10)
    data = []
    if len(raw) == 10:
        data = [
            (raw[1] << 8 | raw[0])/div*fact,
            (raw[3] << 8 | raw[2])/div*fact,
            (raw[5] << 8 | raw[4])/div*fact,
            (raw[7] << 8 | raw[6])/340.0+36.25,
            (raw[9] << 8 | raw[8])]
        return data
    return False

# Read Config
config = configparser.ConfigParser()
config.read('gyro.ini')

# Set logs
logging.basicConfig(format='%(asctime)s %(message)s',
                    filename=config['log']['filename'],
                    level=logging.DEBUG)
logging.info("Welcome to OSM_clinostat gyro push to MQTT")

# Connect to MQTT
logging.info("Connect to mqtt server on '{}'".format(config['mqtt']['host']))
mqtt_client = connect_to_mqtt(config['mqtt']['client_name'],
                              config['mqtt']['host'],
                              config['mqtt']['port'])

# Discover and connect to bluetooth devices
logging.info("Bluetooth discovery")
bt_addr = scan_bt_devices(config['bluetooth']['device_name'])
logging.info("Select device {} with address: {}".format(
                config['bluetooth']['device_name'], bt_addr))
logging.info("connect to bluetooth device {}".format(
                config['bluetooth']['device_name']))
bt_sock = connect_to_bt(bt_addr, config['bluetooth']['port'])

# Start loop on gyro socket
logging.info("Start loop on bluetooth device reading")
while True:
    type = None
    s = bt_sock.recv(1)
    if s == b'\x55':
        type = bt_sock.recv(1)
        if type == b'\x51':
            bt_data = read_data(bt_sock, 32768.0, 16)
            if bt_data:
                mqtt_client.publish('{}/{}'.format(
                    config['mqtt']['topic'],
                    config['mqtt']['accel_section']), '{}'.format(bt_data))
        elif type == b'\x52':
            bt_data = read_data(bt_sock, 32768.0, 2000)
            if bt_data:
                mqtt_client.publish('{}/{}'.format(
                    config['mqtt']['topic'],
                    config['mqtt']['gyro_section']), '{}'.format(bt_data))
        elif type == b'\x53':
            bt_data = read_data(bt_sock, 32768.0, 180)
            if bt_data:
                mqtt_client.publish('{}/{}'.format(
                    config['mqtt']['topic'],
                    config['mqtt']['angle_section']), '{}'.format(bt_data))
