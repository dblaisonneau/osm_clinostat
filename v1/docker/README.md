OSM Open Clinostat - Docker
===================

Install Docker and Docker-compose
------------------------

Follow Docker official procedure to install docker-engine and docker-compose

- https://docs.docker.com/install/linux/docker-ce/debian/
- https://docs.docker.com/compose/install/

```bash
sudo apt-get update
sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg2 \
    software-properties-common
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -
sudo add-apt-repository \
   "deb [arch=armhf] https://download.docker.com/linux/debian \
   $(lsb_release -cs) \
   stable"
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io
sudo groupadd docker
sudo usermod -aG docker $USER
sudo systemctl enable docker
sudo curl -L "https://github.com/docker/compose/releases/download/1.25.4/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
```


Clone the project
-----------------

```bash
git clone https://gitlab.com/dblaisonneau/osm_clinostat.git
cd osm_clinostat/docker
```

Configure
-----------

Edit `stepper_control/stepper.ini`

```ini
[motor1]
enable_pin = 14
direction_pin = 15
pull_pin = 18

[motor2]
enable_pin = 2
direction_pin = 3
pull_pin = 4

[mqtt]
host = localhost
port = 1883

[log]
filename = stepper.log
level = DEBUG
```

Start
---------

```bash
docker-compose up -d
docker-compose ps
docker-compose logs -f
```
