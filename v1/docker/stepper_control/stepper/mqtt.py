"""Stepper mqtt class."""

import json
import logging
import paho.mqtt.client as mqtt

CLIENT_ID_SUB = "osm_clinostat_stepper_SUB"
CLIENT_ID_PUB = "osm_clinostat_stepper_PUB"


# pylint: disable=W0613
def on_connect_sub(_client, userdata, flag, rcode):
    """Set callback on connect for a subscribe."""
    mqttc = userdata['client']
    _client.subscribe(mqttc.topics['listen'])


# pylint: disable=W0613
def on_connect_pub(_client, userdata, flag, rcode):
    """Set callback on connect for a publish."""
    mqttc = userdata['client']
    mqttc.log('on_connect :: publish')
    _client.publish(mqttc.topics['log'], "stepper controller connected")


def on_message(_client, userdata, message):
    """Set callback on a message."""
    mqttc = userdata['client']
    if '/' in message.topic:
        motor = message.topic.split('/')[1:]
        mqttc.log("Received '{}' about '{}'".format(message.payload,
                                                    motor[0]))
        mqttc.on_message(motor, message.payload)
    else:
        mqttc.log("Received '{}' with bad topic '{}'".format(message.payload,
                                                             message.topic),
                  "error")


def on_disconnect(_client, userdata, rcode):
    """Set callback on disconnect."""
    mqttc = userdata['client']
    if rcode != 0:
        mqttc.log("Unexpected disconnection.")
    else:
        mqttc.log("Disconnection.")


class MqttClient():
    """MQTT Client to communicate with the other modules."""

    topics = {'log': 'log', 'listen': 'stepper/#'}

    def __init__(self, server, motors, port=1883):
        """Prepare the client."""
        self.server = server
        self.port = port
        self.logger = logging.getLogger("stepper")
        self.motors = motors
        self.mqtt_clients = {'pub': None, 'sub': None}
        self._mqtt_init()

    def on_message(self, motor, message):
        """Manage MQTT messages."""
        self.log("receive {} for #{}".format(message, motor))
        motor_id = motor[0]
        if motor_id not in self.motors.keys():
            self.log("Message received about unknown motor "
                     "'{}'".format(motor_id),
                     "error")
            return False
        if len(motor) > 1:
            return self._on_message_ascii(motor, motor_id, message)
        return self._on_message_json(motor, motor_id, message)

    def _on_message_ascii(self, motor, motor_id, message):
        """Manage MQTT ascii messages."""
        msg = message.decode('ascii')
        action = motor[1]
        if action == 'speed':
            self.motors[motor_id].run(int(msg))
        elif action == 'increment':
            self.motors[motor_id].set_frequency_increment(int(msg))
        elif action == 'action':
            self._on_message_subaction(motor_id, msg)
        else:
            self.log("Unknown action '{}'".format(action), "error")
        return True

    def _on_message_json(self, motor, motor_id, message):
        """Manage MQTT json messages."""
        try:
            msg = json.loads(message)
        except json.decoder.JSONDecodeError:
            self.log("Can not json parse '{}'".format(message),
                     "error")
            return False
        if not isinstance(msg, dict):
            self.log("Bad message format '{}'".format(msg), "error")
        elif 'speed' in msg.keys():
            if not isinstance(msg['speed'], int):
                self.log("Bad speed format '{}'".format(msg),
                         "error")
                return False
            self.motors[motor_id].run(msg['speed'])
        elif 'increment' in msg.keys():
            if not isinstance(msg['increment'], int):
                self.log("Bad increment format '{}'".format(msg),
                         "error")
                return False
            self.motors[motor_id].set_frequency_increment(
                msg['increment'])
        elif 'action' in msg.keys():
            self._on_message_subaction(motor_id, msg['action'])
        else:
            self.log("Unknown message '{}'".format(msg), "error")
        return True

    def _on_message_subaction(self, motor_id, subaction):
        """Manage MQTT subaction messages."""
        if subaction == 'start':
            self.motors[motor_id].run()
        elif subaction == 'stop':
            self.motors[motor_id].stop()
        elif subaction == 'increment':
            self.motors[motor_id].speed_change(1)
        elif subaction == 'decrement':
            self.motors[motor_id].speed_change(-1)
        else:
            self.log("Action unknown '{}'".format(subaction), "error")

    def _mqtt_init(self):
        """Prepare mqtt callbacks."""
        self.userdata = {
            'client': self,
            'listen_topic': self.topics['listen'],
            'log_topic': self.topics['log'],
            'motors': self.motors,
        }

        self.mqtt_clients['pub'] = mqtt.Client(CLIENT_ID_PUB)
        self.mqtt_clients['pub'].on_connect = on_connect_pub
        self.mqtt_clients['pub'].on_disconnect = on_disconnect
        self.mqtt_clients['pub'].user_data_set(self.userdata)
        self.mqtt_clients['pub'].connect(self.server, self.port)

        self.mqtt_clients['sub'] = mqtt.Client(CLIENT_ID_SUB)
        self.mqtt_clients['sub'].on_connect = on_connect_sub
        self.mqtt_clients['sub'].on_disconnect = on_disconnect
        self.mqtt_clients['sub'].on_message = on_message
        self.mqtt_clients['sub'].user_data_set(self.userdata)
        self.mqtt_clients['sub'].connect(self.server, self.port)
        self.send2log("Stepper controller ready")
        self.mqtt_clients['sub'].loop_forever()

    def send2log(self, msg):
        """Send a message to log and MQTT log_topic."""
        self.log('send "{}" to mqtt topic {}'.format(msg, self.topics['log']))
        self.mqtt_clients['pub'].publish(self.topics['log'], msg)

    def log(self, msg, level='info'):
        """Log a message."""
        msg = 'mqtt >> {}'.format(msg)
        if level == 'debug':
            self.logger.debug(msg)
        elif level == 'info':
            self.logger.info(msg)
        elif level == 'warning':
            self.logger.warning(msg)
        elif level == 'error':
            self.logger.error(msg)
        elif level == 'critical':
            self.logger.critical(msg)
