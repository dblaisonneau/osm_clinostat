#!/usr/bin/env python3

"""Stepper control class."""

import logging
try:
    import RPi.GPIO as GPIO
except RuntimeError:
    print('RPi.GPIO can not be loaded without RPi, for pytest only')
    GPIO=print


class StepperController():
    """
    Stepper controller.

    This class controller a stepper motor using a Microstep driver
    """

    MAX_FREQ = 10000

    def __init__(self, name, enable, direction, pull):
        """
        Stepper controller definition.

        :param name: motor name
        :type name: string
        :param enable: enable gpio pin
        :type enable: int
        :param direction: dir gpio pin
        :type direction: int
        :param pull: pull gpio pin
        :type pull: int
        :type enable: int
        """
        self.name = name
        self.pins = {'enable': int(enable),
                     'direction': int(direction),
                     'pull': int(pull)}
        self.pwm = None
        self.frequency = 1
        self.incr = 0
        self.log = logging.getLogger("stepper")
        self._gpio_config()
        self._log('initialized')

    def __repr__(self):
        """
        Return a human representation of this class.

        :return: human representation of this class
        :rtype: string
        """
        return (f"Stepper controller on pins: \n"
                f"  Enable: {self.pins['enable']}\n"
                f"  Dir: {self.pins['direction']}\n"
                f"  Pull: {self.pins['pull']}\n"
                f"frequency to {self.frequency} Hz "
                f"[incr +-{self.incr}]")

    def run(self, freq=1):
        """
        Run the motor.

        :param freq: frequency of steps, in hertz
        :type freq: int
        """
        self.frequency = freq
        self._change_dir(freq > 0)
        self.pwm.start(0.5)
        self.pwm.ChangeFrequency(abs(freq))
        self._log('run motor to {}Hz'.format(freq))

    def stop(self):
        """Stop the motor."""
        self.pwm.stop()
        self._log('stop motor')

    def set_frequency_increment(self, incr):
        """
        Set frequency step.

        :param incr: incr/decrement frequency
        :type incr: integer
        """
        self.incr = incr
        self._log('set increment to {}'.format(incr))

    def speed_change(self, direction = 1):
        """
        Inc/Decrement frequency.

        :param direction: increment or decrement speed
        :type direction: 1 or -1
        """
        new_freq = self.frequency + direction * self.incr
        if abs(new_freq) < self.MAX_FREQ:
            self.frequency = new_freq
            self.run(self.frequency)
            self._log('set frequency to {}Hz'.format(self.frequency))
        else:
            self._log(f'increment to {new_freq}Hz impossible'
                      f' (> {self.MAX_FREQ})')

    def cleanup(self):
        """Release the GPIO pins."""
        GPIO.cleanup([self.pins['enable'],
                      self.pins['direction'],
                      self.pins['pull']])
        self._log('cleanup GPIOs')

    def _log(self, msg, level='info'):
        """Log a message."""
        msg = '{} >> {}'.format(self.name, msg)
        if self.log:
            if level == 'debug':
                self.log.debug(msg)
            elif level == 'info':
                self.log.info(msg)
            elif level == 'warning':
                self.log.warning(msg)
            elif level == 'error':
                self.log.error(msg)
            elif level == 'critical':
                self.log.critical(msg)

    def _change_dir(self, direction):
        """
        Change the direction of the motor.

        :param direction: 0 for default, 1 for reverse
        """
        if direction:
            GPIO.output(self.pins['direction'], GPIO.HIGH)
        else:
            GPIO.output(self.pins['direction'], GPIO.LOW)
        self._log('change direction')

    def _gpio_config(self):
        """Configure all GPIOs."""
        GPIO.setmode(GPIO.BCM)
        GPIO.setwarnings(False)
        GPIO.setup(self.pins['enable'], GPIO.OUT, initial=GPIO.LOW)
        GPIO.setup(self.pins['direction'], GPIO.OUT, initial=GPIO.LOW)
        GPIO.setup(self.pins['pull'], GPIO.OUT, initial=GPIO.LOW)
        self.pwm = GPIO.PWM(self.pins['pull'], 1)
        self._log('GPIO configured')
