#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright 2019 Orange
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

import logging
import pytest
import stepper.stepper


logging.basicConfig(format='%(asctime)s %(message)s',
                    level=logging.DEBUG)

def test_init(mocker):
    mocker.patch('stepper.stepper.GPIO')
    sc = stepper.stepper.StepperController("aaa", 1, 2, 3)
    assert sc.name == "aaa"
    assert sc.pins == {'enable': 1, 'direction': 2, 'pull': 3}

def test_repr(mocker):
    mocker.patch('stepper.stepper.GPIO')
    sc = stepper.stepper.StepperController("aaa", 1, 2, 3)
    r = repr(sc)
    assert "Stepper controller on pins" in r
    assert "Enable: 1" in r
    assert "Dir: 2" in r
    assert "Pull: 3" in r

@pytest.mark.parametrize("freq", [5, 500, -500])
def test_run(freq, mocker, caplog):
    mocker.patch('stepper.stepper.GPIO')
    sc = stepper.stepper.StepperController("aaa", 1, 2, 3)
    sc.run(freq)
    assert sc.frequency == freq
    assert f'run motor to {freq}Hz' in caplog.text

def test_stop(mocker, caplog):
    mocker.patch('stepper.stepper.GPIO')
    sc = stepper.stepper.StepperController("aaa", 1, 2, 3)
    sc.run()
    sc.stop()
    assert "stop motor" in caplog.text

@pytest.mark.parametrize("incr", [5, 50])
def test_set_frequency_increment(incr, mocker, caplog):
    mocker.patch('stepper.stepper.GPIO')
    sc = stepper.stepper.StepperController("aaa", 1, 2, 3)
    sc.set_frequency_increment(incr)
    assert sc.incr == incr
    assert f'set increment to {incr}' in caplog.text

@pytest.mark.parametrize("dir", [-1, 1])
@pytest.mark.parametrize("incr", [5, 50])
def test_speed_change(incr, dir, mocker, caplog):
    mocker.patch('stepper.stepper.GPIO')
    sc = stepper.stepper.StepperController("aaa", 1, 2, 3)
    sc.run()
    sc.set_frequency_increment(incr)
    sc.speed_change(dir)
    assert sc.frequency == 1 + (dir * incr)
    assert f'run motor to {sc.frequency}Hz' in caplog.text

@pytest.mark.parametrize("dir", [-1, 1])
def test_speed_change_out_of_band(dir, mocker, caplog):
    freq = 100
    mocker.patch('stepper.stepper.GPIO')
    sc = stepper.stepper.StepperController("aaa", 1, 2, 3)
    sc.run(freq)
    sc.set_frequency_increment(50000)
    sc.speed_change(dir)
    assert sc.frequency == freq
    assert f'increment to {100+(dir*50000)}Hz impossible' in caplog.text

def test_cleanup(mocker, caplog):
    mocker.patch('stepper.stepper.GPIO')
    sc = stepper.stepper.StepperController("aaa", 1, 2, 3)
    sc.run()
    sc.cleanup()
    assert stepper.stepper.GPIO.cleanup._mock_call_count == 1
    assert "cleanup GPIO" in caplog.text

@pytest.mark.parametrize("lvl", ['debug', 'info', 'warning',
                                 'error', 'critical'])
def test_log(lvl, mocker, caplog):
    mocker.patch('stepper.stepper.GPIO')
    sc = stepper.stepper.StepperController("aaa", 1, 2, 3)
    sc._log('foobar', lvl)
    assert "foobar" in caplog.text
