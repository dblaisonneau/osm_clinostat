#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright 2019 Orange
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

import logging
import pytest
import stepper.mqtt
from unittest.mock import MagicMock


@pytest.fixture
def motors():
    return {"motor1": MagicMock(), "motor2": MagicMock()}


def test_client_init(motors, mocker, caplog):
    mocker.patch('stepper.mqtt.mqtt')
    mqttc = stepper.mqtt.MqttClient('localhost', motors)
    assert mqttc.server == 'localhost'
    assert mqttc.port == 1883
    assert stepper.mqtt.mqtt.Client._mock_call_count == 2
    assert "Stepper controller ready" in caplog.text


def test_on_connect_pub(mocker):
    client = MagicMock()
    client.topics = {'listen': 'foo'}
    userdata = {'client': client}
    stepper.mqtt.on_connect_sub(client, userdata, None, None)
    client.subscribe.assert_called_once_with('foo')


def test_on_connect_sub(mocker):
    client = MagicMock()
    client.topics = {'log': 'foo'}
    userdata = {'client': client}
    stepper.mqtt.on_connect_pub(client, userdata, None, None)
    client.publish.assert_called_once_with('foo',
                                           "stepper controller connected")


def test_on_message(mocker):
    client = MagicMock()
    client.topics = {'log': 'foo'}
    userdata = {'client': client}
    message = MagicMock()
    message.topic = '/foo/bar'
    message.payload = 'lipsum'
    stepper.mqtt.on_message(client, userdata, message)
    client.on_message.assert_called_once_with(['foo', 'bar'], 'lipsum')
    client.log.assert_called_once()


def test_on_message_bad_topic(mocker):
    client = MagicMock()
    client.topics = {'log': 'foo'}
    userdata = {'client': client}
    message = MagicMock()
    message.topic = 'foo_bar'
    message.payload = 'lipsum'
    stepper.mqtt.on_message(client, userdata, message)
    client.log.assert_called_once_with("Received 'lipsum' with bad topic 'foo_bar'", 'error')


@pytest.mark.parametrize("rc", [0, 1])
def test_on_disconnect(mocker, rc):
    client = MagicMock()
    userdata = {'client': client}
    stepper.mqtt.on_disconnect(client, userdata, rc)
    if rc != 0:
        client.log.assert_called_once_with("Unexpected disconnection.")
    else:
        client.log.assert_called_once_with("Disconnection.")


def test_client_on_message_with_ascii(mocker, motors):
    mocker.patch('stepper.mqtt.mqtt')
    mqttc = stepper.mqtt.MqttClient('localhost', motors)
    mqttc.log = MagicMock()
    mqttc._on_message_ascii = MagicMock()
    motor_id = list(motors.keys())[0]
    motor = [motor_id, 'foo']
    message = 'lipsum'
    mqttc.on_message(motor, message)
    mqttc.log.assert_called_once_with(f"receive {message} for #{motor}")
    mqttc._on_message_ascii.assert_called_once_with(motor, motor_id, message)


def test_client_on_message_with_json(mocker, motors):
    mocker.patch('stepper.mqtt.mqtt')
    mqttc = stepper.mqtt.MqttClient('localhost', motors)
    mqttc.log = MagicMock()
    mqttc._on_message_json = MagicMock()
    motor_id = list(motors.keys())[0]
    motor = [motor_id]
    message = rb'lipsum'
    mqttc.on_message(motor, message)
    mqttc.log.assert_called_once_with(f"receive {message} for #{motor}")
    mqttc._on_message_json.assert_called_once_with(motor, motor_id, message)


def test_client_on_message_bad_motor(mocker, motors):
    mocker.patch('stepper.mqtt.mqtt')
    mqttc = stepper.mqtt.MqttClient('localhost', motors)
    mqttc.log = MagicMock()
    motor_id = 'bad_motor'
    message = rb'lipsum'
    motor = [motor_id]
    assert mqttc.on_message(motor, message) is False
    mqttc.log.assert_called_with("Message received about unknown "
                                 f"motor '{motor_id}'", "error")


def test_client_on_message_ascii_speed(mocker, motors):
    mocker.patch('stepper.mqtt.mqtt')
    mqttc = stepper.mqtt.MqttClient('localhost', motors)
    motor_id = list(motors.keys())[0]
    motor = [motor_id, 'speed']
    message = rb"5"
    mqttc._on_message_ascii(motor, motor_id, message)
    motors[motor_id].run.assert_called_with(5)


def test_client_on_message_ascii_increment(mocker, motors):
    mocker.patch('stepper.mqtt.mqtt')
    mqttc = stepper.mqtt.MqttClient('localhost', motors)
    motor_id = list(motors.keys())[0]
    motor = [motor_id, 'increment']
    message = rb"5"
    mqttc._on_message_ascii(motor, motor_id, message)
    motors[motor_id].set_frequency_increment.assert_called_with(5)


def test_client_on_message_ascii_action(mocker, motors):
    mocker.patch('stepper.mqtt.mqtt')
    mqttc = stepper.mqtt.MqttClient('localhost', motors)
    mqttc._on_message_subaction = MagicMock()
    motor_id = list(motors.keys())[0]
    motor = [motor_id, 'action']
    message = rb"lipsum"
    mqttc._on_message_ascii(motor, motor_id, message)
    mqttc._on_message_subaction.assert_called_once_with(motor_id, message.decode('ascii'))


def test_client_on_message_ascii_bad_action(mocker, motors):
    mocker.patch('stepper.mqtt.mqtt')
    mqttc = stepper.mqtt.MqttClient('localhost', motors)
    mqttc.log = MagicMock()
    motor_id = list(motors.keys())[0]
    motor = [motor_id, 'bad_action']
    message = rb"lipsum"
    mqttc._on_message_ascii(motor, motor_id, message)
    mqttc.log.assert_called_with(f"Unknown action '{motor[1]}'", 'error')
