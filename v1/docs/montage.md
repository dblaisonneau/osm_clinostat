# OSM Open Clinostat - Montage


## Préparation

## Impressions

## Découpes

### Pièces du châssis

### Cornières de renfort

### Plaques de supports

### Plaque de rigidification des pieds

## Perçage / Taraudage

### Cadre

### Charnières

### Cornières de renfort

### Perçage des plaques de support

## Montage du cadre

## Montage des pieds / Charnières

## Passage des fils
