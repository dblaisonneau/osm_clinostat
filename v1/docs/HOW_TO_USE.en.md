# OpenClinostat

A project by Open Space Maker Federation and Fablab Lannion
(www.fablab-lannion.org)

## Project sources

https://www.federation-openspacemakers.com/fr/participer/projets/open-clinostat-3d/

OpenClinostat 3D - Fablab Lannion iteration sources:

## Price

**less than 500€**

## Summary

A clinostat is a device which uses rotation to negate the effects of
gravitational pull on plant growth (gravitropism) and development
(gravimorphism). It has also been used to study the effects of microgravity on
cell cultures, animal embryos and spider webs.
-- [Wikipedia](https://en.wikipedia.org/wiki/Clinostat) --

## Unfold the Clinostat

Open each side legs of the Clinostat and screw in the legs with provided
molette' as in next picture.

![molette](images/molette.jpg)

## Starting the Clinostat

Just plug-in the power cable.

## Controle to the Clinostat

The Clinostat control is done using a set of MQTT message bus over webSocket.
For demo purpose, the Clinostat handle a simple web page to manage those
messages.

This interface is pretty simple and allow the control of the 2 axis of the
Clinostat. Each motor can be enable or disable and the rotation speed factor
set from `0` to `1000`

To connect the Clinostat, use:
- the `ethernet` link and connect to page `http://clinopi:8080`
- the `wifi` link with SSID:`Clinostat` and wifi password `OSM4ever` and the
- connect to `http://192.168.4.1:8080`

![control](images/web_screenshot.png)

## Power off

Press the button behing the electronic board, wait for about a minute then
unplug the power cable.