Shopping list
=============

__Total: 570,66__

A ajouter: Plexy + cornieres alu (Mr Brico)

Structure
-----------

### [Motedis](https://www.motedis.fr/)

| Qty  | Type  | Unit Price  | Total Price  |
|---|---|---|---|
| 4  | [Tasseau lourd rainure 10 B-Typ [M6]](https://www.motedis.fr/shop/Profile-accessoires/Accessoires-45-B-Type-rainure-10/Ecrous-fendus-ecrous-marteaux-boulons-marteaux-pour-rainure-10-type-B/Tasseau-lourd-rainure-10-B-Typ-M6::999994601.html) | 0,3203 € | 1,28 € |
| 4  | [Profilé aluminium 45x45L B-Typ rainure 10 Longueur: 	1980 mm](https://www.motedis.fr/shop/Profile-a-rainures/Profile-45-B-Type-rainure-10/Profil%E9-aluminium-45x45L-B-Typ-rainure-10::99999416.html)  | 16,9411 € |	67,76 €|
| 1  | [100 x	Ecrou à tête marteau rainure 10 B-Type [M6]](https://www.motedis.fr/shop/Profile-accessoires/Accessoires-45-B-Type-rainure-10/Ecrous-fendus-ecrous-marteaux-boulons-marteaux-pour-rainure-10-type-B/Ecrou-%E0-t%EAte-marteau-rainure-10-B-Type-M6::999994592.html) | 0,1127 € | 11,27 € |
| 1  | [100 x	Vis DIN 7380 grandeur: 	M6x12](https://www.motedis.fr/shop/Base-mechaniques/Basic-Basics/DIN-element-normalise/Vis-DIN/Vis-DIN-7380::999991070.html) | 0,1230 € | 12,30 € |
| 1  | [100 x	Vis autotaraudeuse S12x30 T50 P45 B-type rainure 10](https://www.motedis.fr/shop/Profile-accessoires/Accessoires-45-B-Type-rainure-10/Connecteur-et-angle-rainure-10-Type-B/Vis-autotaraudeuse-S12x30-T50-P45-B-type-rainure-10::999991508.html) | 35,8750 € | 35,88 € |

- Price without taxes:	128,49 €
- Shipping:	21,93 €
- 20% Taxes:	30,09 €
- __Total:	180,51 €__

### Monsieur Bricolage

| Qty  | Type  | Unit Price  | Total Price  |
|---|---|---|---|
| 3 |[Cornière Alu 40x40x1500](https://www.mr-bricolage.fr/Lannion/corniere-alu-naturel-argent-mm-40x40x1-5-arcansas.html)|7,99| 23,97|
| 4 |[Charnière à congés](https://www.mr-bricolage.fr/Lannion/catalog/product/view/id/54078/s/charniere-a-conge/) |3,99€| 15,96€|
| 1 |[Plat alu naturel argent mm.40x2](https://www.mr-bricolage.fr/Lannion/plat-alu-naturel-argent-mm-40x2-arcansas.html) | 5,99  |  5,99  |
| 1  | [tube alu Diam 10]   |  6 ? | 6 ?  |

- __Total:	55,92 €__

Electronic
-------------

| Qty  | Type  | Unit Price  | Total Price  |
|---|---|---|---|
| 2 | [Bague collectrice 6 fils](https://fr.aliexpress.com/item/32970770622.html?spm=a2g0s.9042311.0.0.4b706c37eXKAtb)  |   | € 75,91 (ship incl) |
| 2 | [Nema17 Moteur à engrenages 1.68A 19:1](https://www.amazon.fr/gp/product/B077ZP7NX3/ref=ppx_yo_dt_b_asin_title_o01_s00?ie=UTF8&psc=1)  | 42,90  | 84,80 € |
| 1 | [NEMA 17 Moteur pas Stepper moteur pas à pas Driver, 3PCS TB6600 ](https://www.amazon.fr/gp/product/B07D6L9FM1/ref=ppx_yo_dt_b_asin_title_o02_s00?ie=UTF8&psc=1) |   | 32,99 € |
| 1 | [transformateur 24V 300W PSU - NL300-H1V24](https://www.amazon.fr/gp/product/B07HSY4TM9/ref=ppx_yo_dt_b_asin_title_o03_s00?ie=UTF8&psc=1)  |   |  35,99 € |
| 1 | [Convertisseur DC 12V 24V à 5V 10A 50W](https://www.amazon.fr/DUMVOIN-Convertisseur-R%C3%A9gulateur-Alimentations-Transformateur/dp/B075S7GHH1/ref=sr_1_6?__mk_fr_FR=%C3%85M%C3%85%C5%BD%C3%95%C3%91&keywords=convertisseur+24v+5v&qid=1573081160&sprefix=convertis%2Caps%2C296&sr=8-6)  |   | 16,68 €  |
| 1 | [40pcs 20cm 2,54 mm mâle à femelle Dupont fil ](https://www.amazon.fr/Neuftech%C2%AE-femelle-Dupont-Arduino-Breadboard/dp/B00M9XOCV4/ref=sr_1_10?__mk_fr_FR=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=17Q79WTXYDOE4&keywords=fils+dupont&qid=1573081405&sprefix=fils+dup%2Caps%2C230&sr=8-10)  |   | 3,99 €  |
| 1 | [Raspberry Pi Carte Mère 3 Model B](https://www.amazon.fr/Raspberry-Pi-Carte-M%C3%A8re-Model/dp/B01CD5VC92/ref=sr_1_3?__mk_fr_FR=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=XA9TGFVA23HS&keywords=raspberry+pi+3&qid=1573081356&sprefix=ras%2Caps%2C221&sr=8-3)  |   | 32,10€ |

- __Total:	334,27 €__
