Hardware
================

Le clinostat se compose de 2 plateaux tournants sur 2 axes perpendiculaires
au sein d'un cadre sur pieds. Le tout est préparé pour être à la fois
transportable mais aussi résistant.

Voici une vue d'ensemble:

![vue globale](images/render_global.png)

Vous pourrez retrouver toute la liste des achats ici:
[Liste d'achat](shopping_list.md)


B.O.M - Liste du matériel
========================

Découpe des profilés 45x45
-----------------

![vue du dessus](images/render_top_cotes.png)

- cadre extérieur:
  - 671mm x2
  - 390mm x4
- cadre intermédiaire:
  - 286mm x4
  - 240mm x4
- cadre intérieur:
  - 186mm x4
  - 480mm x2
- pieds
  - 671mm x3
  - 350mm x4

- 8 x Cornières 40x40 de 500mm
- 4 barres de contreventement suivant le modèle suivant:

![contreventement](images/rigidificateur.JPG)

Visserie
-------------

- 16 vis S12x30
- 80 vis M6x12
- 80 écrous à tête marteau rainure 10 B-Type
- 4 charnières à congés

Assemblage
==================

Cadre - Alu contre Alu:
-------------------------

L'assemblage des profilés entre eux se fait avec des vis auto-taraudeuses
comme sur le schéma suivant fourni par le vendeur (Motedis).

![vue du dessus](images/vis_autotaraudeuse.jpg)

Pour cela percer le profil supérieur (celui où se loge la tête de vis) à 22,5mm
du bord avec un foret de 9 ou 10 mm. Glisser la vis auto-taraudeuse dans la
rainure et visser dans le cœur de l'autre profilé à l'aide d'un tournevis
Torx 50 (ou une visseuse avec embout T50 suffisamment long)

Il y a en tout 16 assemblages à faire de cette manière.

Au niveau des axes
---------------------

Les axes de chaque plateau sont montés grâce à des pièces imprimées hébergeant
les moteurs ou les bagues collectrices.

Entre chaque plateau il y a:

- 1 assemblage moteur:
  - 1 pièce "coque nema"
  - 1 pièce "pivot nema"
  - 1 moteur "nema17"
  - 1 boulon pour bloquer le meplat de l'axe moteur
  - 2 cornières alu en L (40x40) de 50cm
  - 16 vis M6x12
  - 16 écrous à tête marteau rainure 10 B-Type [M6]

![assemblage moteur](images/photos/20191030_225111.jpg)
![assemblage moteur 2](images/photos/20191030_225047.jpg)

- 1 assemblage avec bague collectrice
  - 1 pièce "Support_axe_central"
  - 1 pièce "Support_axe_central_1_fixe"
  - 1 bague collectrice
  - 1 baguette alu de 12mm
  - 2 cornières alu en L (40x40) de 50cm
  - 16 vis M6x12
  - 16 écrous à tête marteau rainure 10 B-Type [M6]

![assemblage moteur](images/photos/20191030_225101.jpg)

Sur chaque assemblage, les 2 profilés alu sont assemblés par une
pièce imprimée, puis maintenus par une cornière en L. Chaque cornière percée
8 fois à 22,5mm de la pliure (ou 17,5mm du bord pour une cornière de 40) avec
un foret de diametre 6, comme sur la photo suivante:

![cornière](images/corniere.jpg)

Contrairement à la photo où l'assemblage s'est fait à l'écrou, l'assemblage se
fait à la vis M6x12 et à l'écrou à tête marteau qui doit se glisser dans la
rainure des profilés.

**NB 1:** Il peut être nécessaire de percer un trou supplémentaire pour passer les
câbles

**NB 2:** En cas de doute sur la solidité avec 1 seule cornière, n'hésitez
pas à ajouter un plat alu sur le troisième coté.


Pieds
-----------

Les pieds sont liés au cadre principal par les charnières fixées à la vis M6 et
écrou à tête marteau. Il est possible  qu'il soit nécessaire de refaire des
trous dans les charnières.

![coté](images/pied.png)


Entre eux les pieds sont assemblés pour former 2 structures, une en U et la
seconde en cadre.

![coté](images/render_side.png)

Pour rigidifier la structure une fois ouverte, une barre de contreventement,
aussi appelée "rigidificateur" est positionnée en travers. Cette barre glisse
le long des profilés grâce aux pièces imprimées "chariot_coulissant_" et
"chariot_coulissant_2". Les molettes permettent de serrer les chariots en
utilisant des vis de type M6 à tête hexagonale.

<img src="images/chariot_coulissant_.JPG" width="25%">
<img src="images/chariot_coulissant_2.JPG" width="25%">
<img src="images/molette.JPG" width="25%">
