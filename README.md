OSM Open Clinostat - Made in FabLab Lannion
==================

Ce projet est un fork du projet "Open Clinostat 3D" de la Fédération Open Space
Maker. Voici les pages du projet original:

- [La page du projet sur le site OpenSpaceMaker](https://www.federation-openspacemakers.com/fr/participer/projets/open-clinostat-3d/)
- [La page du fablab Plateforme C](https://fablabo.net/wiki/OPEN_CLINOSTAT_3D)

Ce projet du Fablab reprend vie en cette fin d'année 204 avec une nouvelle
demande d'évolution ! Ce projet est donc à nouveau en évolution.

Vous pouvez retrouver les plans et le code de la version 'historique' du 
Clinostat du Fablab de Lannion en suivant ces liens:
- <https://gitlab.com/fablablannion/osm_clinostat/-/tree/v1?ref_type=tags>
- ou dans le [le dossier v1](./v1/)